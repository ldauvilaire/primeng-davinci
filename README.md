# PrimeNG
Sample application showing PrimeNG with webpack configuration.

![alt text](http://blog.davincisoftware.sk/primeng-web-component-framework-based-on-angularjs-2 "Article describing this sample")

You need NodeJS to run this sample
Execute the following commands to run the sample. Application run on http:\\\\localhost:8080 address.

```
npm install
npm install webpack
npm install webpack-dev-server
npm start
```
